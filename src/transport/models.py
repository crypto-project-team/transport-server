from django.conf import settings
from django.db import models

from transport.utils import get_upload_path


class CloudServer(models.Model):
    host = models.CharField('Хост', max_length=16)
    port = models.IntegerField('Порт')
    user = models.CharField('User', max_length=256)
    password = models.CharField('Password', max_length=256)
    created = models.DateTimeField('Дата создания', auto_now_add=True)

    def __str__(self) -> str:
        return f'host: {self.host}:{self.port}'

    class Meta:
        verbose_name = 'Вычислительный сервер'
        verbose_name_plural = 'Вычислительные сервера'


class Result(models.Model):
    task_id = models.IntegerField('Task', blank=True, null=True)
    result = models.CharField('Ответ', max_length=2048, blank=True, null=True)
    file = models.FileField('Файл', upload_to=get_upload_path)
    created = models.DateTimeField('Дата создания', auto_now_add=True)

    def __str__(self) -> str:
        return f'id: {self.pk}, result: {self.result}'

    class Meta:
        verbose_name = 'Результат'
        verbose_name_plural = 'Результаты'


class Task(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    status = models.CharField('Статус', max_length=64)
    created = models.DateTimeField('Дата создания', auto_now_add=True)
    task = models.CharField('Задача', max_length=2048)
    server = models.ForeignKey(CloudServer, on_delete=models.CASCADE)
    result = models.OneToOneField(Result, on_delete=models.SET_NULL, blank=True, null=True)
    file = models.FileField('Файл', upload_to=get_upload_path, blank=True, null=True)

    def __str__(self) -> str:
        return f'id: {self.pk}, user: {self.user}, status: {self.status}'

    class Meta:
        verbose_name = 'Задача'
        verbose_name_plural = 'Задачи'
