from django.conf import urls
from rest_framework.routers import DefaultRouter

from .views import *

router = DefaultRouter()
router.register('result', ResultViewSet, basename='result')

urlpatterns = [
    urls.url(r'^token-auth/', AuthToken.as_view(name='auth')),
]

urlpatterns += router.urls
