from rest_framework import serializers

from transport.models import *


class ResultSerializer(serializers.ModelSerializer):
    class Meta:
        model = Result
        fields = ('task_id', 'created', 'file')

    def create(self, validated_data, *args, **kwargs):
        task_id = self.context['request'].data['task_id']
        validated_data['task_id'] = task_id
        obj = super().create(validated_data)
        return obj
