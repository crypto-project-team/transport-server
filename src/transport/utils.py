import os
from typing import Tuple

import requests
from django.conf import settings


def get_id_token(server: str, user: str, password: str) -> Tuple[int, str]:
    r = requests.post(f'http://{server}/api/token-auth/', data={'username': f'{user}', 'password': f'{password}'})
    js = r.json()
    return js['id'], js['token']


def get_upload_path(instance, filename):
    return os.path.join(settings.FILES_UPLOAD_TO, filename)


def get_all_file_paths(directory) -> list:
    file_paths = []
    for root, directories, files in os.walk(directory):
        for filename in files:
            filepath = os.path.join(root, filename)
            file_paths.append(filepath)
    return file_paths


class Sender:
    __slots__ = '__token', '__server'
    __token: str
    __server: str

    def __init__(self, token: str, server: str) -> None:
        self.__token = token
        self.__server = server

    @staticmethod
    def __get_id(r: requests) -> int:
        try:
            status = int(r.json()['pk'])
        except KeyError:
            raise Exception(f'status:{r.status_code}, reason:{r.reason}')
        return status

    def send_file(self, file_path: str, task_id: int) -> int:
        url = f'http://{self.__server}/api/upload_calculation/'
        r = requests.post(
            url,
            files={'file': open(file_path, 'rb')},
            data={'task_id': task_id},
            headers={f'Authorization': f'Token {self.__token}'})
        return self.__get_id(r)

    def run_task(self, task_id: int, token: str) -> Tuple[int, dict]:
        url = f'http://{self.__server}/api/run_calculation/'
        r = requests.get(
            url,
            data={'task': task_id, 'token': token},
            headers={f'Authorization': f'Token {self.__token}'})
        return r.status_code, r.json()
