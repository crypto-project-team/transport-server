from django import forms
from django.contrib import admin

from transport.models import *
from transport.tasks import send_task


@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    list_display = ('user', 'task', 'get_result')

    @staticmethod
    def get_result(obj: Task) -> str:
        if not obj.status:
            obj.status = 'sending'
            obj.save()
            # send_task.delay(obj.pk)
            print(f'Send task::{obj}')
            send_task(obj.pk)

        if obj.result is None:
            return obj.status
        else:
            return obj.result.result

    class TaskAddModelForm(forms.ModelForm):
        class Meta:
            fields = ['user', 'server', 'task']
            model = Task

    add_form = TaskAddModelForm
    list_display_links = None

    def get_form(self, request, obj=None, **kwargs):
        defaults = {}
        if obj is None:
            defaults['form'] = self.add_form
        defaults.update(kwargs)
        return super().get_form(request, obj, **defaults)

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return self.readonly_fields
        else:
            return super(TaskAdmin, self).get_readonly_fields(request, obj)


@admin.register(CloudServer)
class CloudServerAdmin(admin.ModelAdmin):
    list_display = ('server', 'created')

    @staticmethod
    def server(obj: CloudServer) -> str:
        return f'{obj.host}:{obj.port}'

    class CloudServerAddModelForm(forms.ModelForm):
        class Meta:
            fields = ['host', 'port', 'user', 'password']
            model = CloudServer

    list_display_links = None
    form_add = CloudServerAddModelForm


@admin.register(Result)
class ResultAdmin(admin.ModelAdmin):
    list_display = ('result', 'task_definition', 'created')

    @staticmethod
    def task_definition(obj: Result):
        return Task.objects.get(pk=obj.task_id).task

    def has_add_permission(self, request, obj=None):
        return False
