import os
import zipfile
from time import time
from typing import List, Tuple

from rest_framework.authtoken.models import Token

from crypto_library import client
from transport.models import *
from transport.utils import Sender, get_all_file_paths, get_id_token


# from transport_server.celery import app as celery_app
# @celery_app.task(name='transport_server.send_task')
def send_task(task_id: int) -> None:
    task: Task = Task.objects.get(pk=task_id)
    print(f'START::Sending task: {task}')
    sending_task(task)
    task.status = 'calculating'
    task.save()
    print(f'END::Sending task: {task}')


def sending_task(task: Task) -> None:
    # encrypting
    exercise: str = task.task
    print(f'Start encrypting {exercise}')
    t_start = time()
    tokens: List[str] = tokenizer(exercise)
    if not tokens:
        raise Exception(f'Bad string tokens is {tokens}')
    nums, ops = return_filtered_token(tokens)
    print(f'Got tokens: {tokens}, nums: {nums}, ops: {ops}')
    exec_path: str = settings.CLIENT_PATH
    dir_name = f'task_{task.pk}'
    dir_path = os.path.join(settings.MEDIA_ROOT, dir_name)
    if not os.path.exists(dir_path):
        os.mkdir(dir_path)
    else:
        raise Exception(f'Bad path for new directory: {dir_path}')
    ep_proc = client.ExpressionProcessor(1e4)
    ep_proc.save_expression(nums, ops, dir_path, exec_path)
    print(f'End encrypting {exercise}, spent:{(time() - t_start) * 1e3:.1f} ms')

    # zipping
    file_paths: list = get_all_file_paths(dir_path)
    zipfile_path = os.path.join(settings.MEDIA_ROOT, f'task_{task.pk}.zip')
    with zipfile.ZipFile(zipfile_path, 'w') as zip_file:
        for file in file_paths:
            zip_file.write(file, os.path.basename(file))

    # sending
    server = task.server
    server_ip = f'{server.host}:{server.port}'
    _, cloud_token = get_id_token(server_ip, server.user, server.password)
    client_token = get_user_token(task.user_id)
    sender = Sender(cloud_token, server_ip)
    task_id = sender.send_file(zipfile_path, task.pk)
    status, req = sender.run_task(task_id, client_token)
    if status != 200:
        raise Exception(f'Task sending failed: {req}')


def return_filtered_token(tokens: List[str]) -> Tuple[List[str], List[str]]:
    nums: List[str] = []
    ops: List[str] = []

    for idx, token in enumerate(tokens):
        if idx % 2:
            ops.append(token)
        else:
            if token.isdigit():
                nums.append(token)
            else:
                raise Exception(f'Bad tokens: {tokens}')
    return nums, ops


def get_user_token(user_id) -> str:
    token = Token.objects.get(user_id=user_id)
    return token.key


def tokenizer(exercise: str) -> List[str]:
    return exercise.split(' ')


# @celery_app.task(name='transport_server.decrypting')
def decrypting(result_id: int) -> None:
    result: Result = Result.objects.get(pk=result_id)
    print(f'START::Decrypting result: {result}')
    t_start = time()
    file = result.file.path
    ep_proc = client.ExpressionProcessor(1e4)
    result.result = int(ep_proc.decrypt_result(file, settings.CLIENT_PATH))
    result.save()
    # write to task
    task: Task = Task.objects.get(pk=result.task_id)
    task.result = result
    task.status = 'complete'
    task.save()
    print(f'END::Decrypting result: {result}, spent:{(time() - t_start) * 1e3:.1f} ms')
