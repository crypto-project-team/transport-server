from django import dispatch
from django.db.models import signals

from transport.tasks import *
from transport_server.utils import auto_checker


@dispatch.receiver(signals.post_save, sender=Result)
def save_result(instance: Result, created: bool, **kwargs):
    if created:
        print(f'Save result::{instance}')
        decrypting(instance.pk)  # decrypting.delay(instance.pk)


@dispatch.receiver(signals.post_save, sender=Task)
def update_result(instance: Task, **kwargs):
    if instance.status == 'complete':
        status = instance.result
        status.result = auto_checker(instance.task)
        status.save()
