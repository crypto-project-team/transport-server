from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from transport.serializers import *


class ResultViewSet(ModelViewSet):
    permission_classes = (IsAuthenticated,)
    queryset = Result.objects.all()
    serializer_class = ResultSerializer
    http_method_names = ['post']


@method_decorator(csrf_exempt, name='dispatch')
class AuthToken(ObtainAuthToken):
    name = None

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']

        token, created = Token.objects.get_or_create(user=user)

        user_groups = user.groups.all()
        groups = [str(g) for g in user_groups]
        permissions = [group.permissions.all() for group in user_groups]
        permissions = [item.codename for sublist in permissions for item in sublist]

        return Response({
            'username': user.username,
            'id': user.id,
            'token': str(token),
            'groups': groups,
            'permissions': permissions,
        })
