from django.contrib import admin
from django.urls import include, path

from transport import urls as transport_urls

urlpatterns = [
    path('api/', include(transport_urls)),
    path('admin/', admin.site.urls),
]
