from typing import List, Tuple


def auto_checker(task: str) -> str:
    tokens = task.split(" ")
    first, second = return_filtered_token(tokens)
    return check_serializable(first, second)


def return_filtered_token(tokens: List[str]) -> Tuple[List[str], List[str]]:
    nums: List[str] = []
    ops: List[str] = []

    for idx, token in enumerate(tokens):
        if idx % 2:
            ops.append(token)
        else:
            if token.isdigit():
                nums.append(token)
            else:
                raise Exception(f'Bad tokens: {tokens}')
    return nums, ops


def check_serializable(first: List[str], second: List[str]) -> str:
    from random import randint
    result = int(first[0])
    for i in range(0, len(second)):
        if second[i] == '+':
            result += int(first[i + 1])
        if second[i] == '-':
            result -= int(first[i + 1])
        if second[i] == '@':
            result *= int(first[i + 1])
    result = result if 0 < result < 1013 else randint(0, 11)
    return str(result)
