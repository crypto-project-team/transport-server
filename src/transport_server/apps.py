from django import apps


class TransportAppConfig(apps.AppConfig):
    name = 'transport_server'

    def ready(self):
        __import__('transport.signals')
